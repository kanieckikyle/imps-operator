import kopf
from utils import load_yaml_from_path
import copy

import glom
from models import ImagePullSecret
from collections import defaultdict

from kubernetes import client


def ensure_cron_job(v1_batch: client.BatchV1Api, cronjob: dict):
    try:
        v1_batch.replace_namespaced_cron_job(
            name=cronjob["metadata"]["name"],
            namespace=cronjob["metadata"]["namespace"],
            body=cronjob,
        )
    except client.ApiException as e:
        if e.status == 404:
            v1_batch.create_namespaced_cron_job(
                namespace=cronjob["metadata"]["namespace"], body=cronjob
            )
        else:
            raise


def ensure_secret(v1_core: client.CoreV1Api, secret: dict):
    try:
        v1_core.replace_namespaced_secret(
            name=secret["metadata"]["name"],
            namespace=secret["metadata"]["namespace"],
            body=secret,
        )
    except client.ApiException as e:
        if e.status == 404:
            v1_core.create_namespaced_secret(
                namespace=secret["metadata"]["namespace"], body=secret
            )
        else:
            raise


def ensure_sa(v1_core: client.CoreV1Api, service_account: dict):
    try:
        v1_core.replace_namespaced_service_account(
            name=service_account["metadata"]["name"],
            namespace=service_account["metadata"]["namespace"],
            body=service_account,
        )
    except client.ApiException as e:
        if e.status == 404:
            v1_core.create_namespaced_service_account(
                namespace=service_account["metadata"]["namespace"], body=service_account
            )
        else:
            raise


def ensure_role(v1_rbac: client.RbacAuthorizationV1Api, role: dict):
    try:
        v1_rbac.replace_namespaced_role(
            name=role["metadata"]["name"],
            namespace=role["metadata"]["namespace"],
            body=role,
        )
    except client.ApiException as e:
        if e.status == 404:
            v1_rbac.create_namespaced_role(
                namespace=role["metadata"]["namespace"], body=role
            )
        else:
            raise


def ensure_role_binding(v1_rbac: client.RbacAuthorizationV1Api, role_binding: dict):
    try:
        v1_rbac.replace_namespaced_role_binding(
            name=role_binding["metadata"]["name"],
            namespace=role_binding["metadata"]["namespace"],
            body=role_binding,
        )
    except client.ApiException as e:
        if e.status == 404:
            v1_rbac.create_namespaced_role_binding(
                namespace=role_binding["metadata"]["namespace"], body=role_binding
            )
        else:
            raise


def create_imps_objects(
    imps: ImagePullSecret,
) -> dict[str, list[dict]]:
    cronjob_tmpl = load_yaml_from_path("./templates/cronjob.yaml")
    secret_tmpl = load_yaml_from_path("./templates/secret.yaml")
    sa_tmpl = load_yaml_from_path("./templates/service_account.yaml")
    role_tmpl = load_yaml_from_path("./templates/role.yaml")
    role_binding_tmpl = load_yaml_from_path("./templates/role-binding.yaml")

    objects = defaultdict(list)
    for namespace in imps.target.namespaces.name:
        sa = copy.deepcopy(sa_tmpl)
        role = copy.deepcopy(role_tmpl)
        role_binding = copy.deepcopy(role_binding_tmpl)
        secret = copy.deepcopy(secret_tmpl)
        cronjob = copy.deepcopy(cronjob_tmpl)

        glom.assign(sa, "metadata.namespace", namespace)
        glom.assign(role, "metadata.namespace", namespace)
        glom.assign(role_binding, "metadata.namespace", namespace)
        glom.assign(cronjob, "metadata.namespace", namespace)
        glom.assign(secret, "metadata.namespace", namespace)

        cronjob_env = glom.glom(
            cronjob, "spec.jobTemplate.spec.template.spec.containers.0.env"
        )

        for var in cronjob_env:
            match var["name"].lower():
                case "name":
                    var["value"] = imps.target.secret.name

        cronjob_env = glom.assign(
            cronjob, "spec.jobTemplate.spec.template.spec.containers.0.env", cronjob_env
        )

        objects["secrets"].append(secret)
        objects["cronjobs"].append(cronjob)
        objects["service_accounts"].append(sa)
        objects["roles"].append(role)
        objects["role_bindings"].append(role_binding)

    return objects


@kopf.on.create("imagepullsecret")
@kopf.on.update("imagepullsecret")
@kopf.on.resume("imagepullsecret")
def ensure_image_pull_secret(spec, **kwargs):
    imps = ImagePullSecret(**spec)

    v1_core = client.CoreV1Api()
    v1_batch = client.BatchV1Api()
    v1_rbac = client.RbacAuthorizationV1Api()

    root_secret = v1_core.read_namespaced_secret(
        imps.registry.credential.name,
        namespace=imps.registry.credential.namespace,
    )

    objects = create_imps_objects(imps)

    for obj in objects.values():
        kopf.adopt(obj)

    for secret in objects.get("secrets", []):
        glom.assign(secret, "data", root_secret.data)
        ensure_secret(v1_core, secret)

    for sa in objects.get("service_accounts", []):
        ensure_sa(v1_core, sa)

    for role in objects.get("roles", []):
        ensure_role(v1_rbac, role)

    for role_binding in objects.get("role_bindings", []):
        ensure_role_binding(v1_rbac, role_binding)

    for cronjob in objects.get("cronjobs", []):
        ensure_cron_job(v1_batch, cronjob)


@kopf.timer("imagepullsecret", interval=60)
def copy_secret_to_cron(spec, **kwargs):
    imps = ImagePullSecret(**spec)
    v1_core = client.CoreV1Api()

    root_secret = v1_core.read_namespaced_secret(
        imps.registry.credential.name,
        namespace=imps.registry.credential.namespace,
    )

    objects = create_imps_objects(imps)

    for secret in objects.get("secrets", []):
        glom.assign(secret, "data", root_secret.data)
        ensure_secret(v1_core, secret)
