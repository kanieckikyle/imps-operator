import yaml


def load_yaml_from_path(path: str) -> dict:
    with open(path, "r") as f:
        return load_yaml_from_str(f.read())


def load_yaml_from_str(s: str) -> dict:
    return yaml.safe_load(s)
