from pydantic import BaseModel


class ImagePullSecretRegistryCredential(BaseModel):

    name: str

    namespace: str


class ImagePullSecretRegistry(BaseModel):

    credential: ImagePullSecretRegistryCredential


class ImagePullSecretTargetSecret(BaseModel):
    name: str


class ImagePullSecretTargetNamespaces(BaseModel):
    name: list[str] = []

    selectors: dict = {}


class ImagePullSecretTarget(BaseModel):
    secret: ImagePullSecretTargetSecret

    namespaces: ImagePullSecretTargetNamespaces


class ImagePullSecret(BaseModel):
    target: ImagePullSecretTarget

    registry: ImagePullSecretRegistry
