import boto3
import json
import base64
from pydantic import BaseModel
from pydantic_settings import BaseSettings, SettingsConfigDict
from kubernetes import client, config

SECRET_TEMPLATE = {
    "apiVersion": "v1",
    "kind": "Secret",
    "type": "kubernetes.io/dockerconfigjson",
    "metadata": {"name": "", "namespace": ""},
    "data": {".dockerconfigjson": ""},
}

docker_config_json = {"auths": {}}


class AwsSecretSettings(BaseModel):
    region: str = "us-east-2"

    registry_id: str = ""

    access_key_id: str = ""

    secret_access_key: str = ""

    session_token: str = ""


class RawSecretSettings(BaseModel):
    region: str = "us-east-2"

    registry_id: str = ""

    access_key_id: str = ""

    secret_access_key: str = ""

    session_token: str = ""


class Settings(BaseSettings):
    model_config = SettingsConfigDict(env_nested_delimiter="__")

    aws: AwsSecretSettings = AwsSecretSettings()

    raw: str = ""
    """json encoded string value of .dockerconfigjson"""

    name: str

    namespace: str

    in_cluster: bool = True


settings = Settings()


def upsert_secret(v1_core: client.CoreV1Api, namespace: str, name: str, body):
    try:
        v1_core.replace_namespaced_secret(namespace=namespace, name=name, body=body)
    except client.ApiException as e:
        if e.status == 404:
            v1_core.create_namespaced_secret(namespace=namespace, body=body)
        else:
            raise


def fetch_ecr_creds() -> dict:
    client_kwargs = {
        "region_name": settings.aws.region,
        "aws_access_key_id": settings.aws.access_key_id,
        "aws_secret_access_key": settings.aws.secret_access_key,
    }
    if settings.aws.session_token:
        client_kwargs.update({"aws_session_token": settings.aws.session_token})

    ecr_client = boto3.client("ecr", **client_kwargs)

    response = ecr_client.get_authorization_token(
        registryIds=[settings.aws.registry_id]
    )

    raw_token = response["authorizationData"][0]["authorizationToken"]
    token = base64.b64decode(raw_token).decode("utf-8")
    username, password = token.split(":")

    return {
        f"https://{settings.aws.registry_id}.dkr.ecr.{settings.aws.region}.amazonaws.com": {
            "username": username,
            "password": password,
            "auth": raw_token,
        }
    }


def main():
    secret_body = SECRET_TEMPLATE.copy()
    secret_body["metadata"]["name"] = settings.name
    secret_body["metadata"]["namespace"] = settings.namespace

    docker_auth = docker_config_json.copy()

    if settings.aws.access_key_id and settings.aws.secret_access_key:
        docker_auth["auths"].update(fetch_ecr_creds(settings))

    elif settings.raw:
        docker_auth["auths"].update(json.loads(settings.raw))

    secret_body["data"][".dockerconfigjson"] = base64.b64encode(
        json.dumps(docker_auth).encode("utf-8")
    ).decode("utf-8")

    # # Load the kubernetes client for incluster config
    if settings.in_cluster:
        config.load_incluster_config()
    else:
        config.load_kube_config("/config/config.yaml")

    v1_core = client.CoreV1Api()

    # Upsert the secret into the namespace
    upsert_secret(v1_core, settings.namespace, settings.name, secret_body)


if __name__ == "__main__":
    main()
